

chVim(){
#config vim
if [ $L = "en" ];then
    x=$(whiptail --title " PveTools   Version : 2.1.3 " --menu "Config VIM:" 12 60 4 \
    "a" "Install vim & simply config display." \
    "b" "Install vim & config 'vim-for-server'." \
    "c" "Uninstall." \
    3>&1 1>&2 2>&3)
else
    x=$(whiptail --title " PveTools   Version : 2.1.3 " --menu "安装配置VIM！" 12 60 4 \
    "a" "安装VIM并简单配置，如配色行号等。" \
    "b" "安装VIM并配置'vim-for-server'。" \
    "c" "还原配置。" \
    3>&1 1>&2 2>&3)
fi
exitstatus=$?
if [ $exitstatus = 0 ]; then
    case "$x" in 
        a )
        if(whiptail --title "Yes/No Box" --yesno "
Install vim & simply config display.Continue?
安装VIM并简单配置，如配色行号等，基本是vim原味儿。是否继续？
            " 10 60) then
            if [ ! -f /root/.vimrc ] || [ `cat /root/.vimrc|wc -l` = 0 ] || [ `dpkg -l |grep vim|wc -l` = 0 ];then
                apt-get -y install vim
            else
                cp ~/.vimrc ~/.vimrc.bak
            fi
            {
            echo 10
            echo 50
            $(
            cat << EOF > ~/.vimrc
set number
set showcmd
set incsearch
set expandtab
set showcmd
set history=400
set autoread
set ffs=unix,mac,dos
set hlsearch
set shiftwidth=2
set wrap
set ai
set si
set cindent
set termencoding=unix
set tabstop=2
set nocompatible
set showmatch
set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936
set termencoding=utf-8
set encoding=utf-8
set fileformats=unix
set ttyfast
syntax on
set imcmdline
set previewwindow
set showfulltag
set cursorline
set ruler
color ron
autocmd InsertEnter * se cul
set ruler
set showcmd
set laststatus=2
set tabstop=2
set softtabstop=4
inoremap fff <esc>h
autocmd BufWritePost \$MYVIMRC source \$MYVIMRCi
EOF
            )
            echo 100
            }|whiptail --gauge "installing" 10 60
            whiptail --title "Success" --msgbox "
    Install & config complete!
    安装配置完成!
            " 10 60
        else
            chVim
        fi
            ;;
        b | B )
        if(whiptail --title "Yes/No Box" --yesno "
安装VIM并配置 \'vim-for-server\'(https://github.com/wklken/vim-for-server).
yes or no?
            " 12 60) then
            echo "Use curl or git? If one not work,change to another."
            echo "选择git或curl，如果一个方式不行可以换一个。"
            echo "1 ) git"
            echo "2 ) curl"
            echo "Please choose:"
            read x
            case $x in 
                2 )
                    apt-get -y install curl vim
                    cp ~/.vimrc ~/.vimrc_bak
                    curl https://raw.githubusercontent.com/wklken/vim-for-server/master/vimrc > ~/.vimrc
                    whiptail --title "Success" --msgbox "
            Install & config complete!
            安装配置完成！
                    " 10 60
                    ;;
                1 | "" )
                    apt-get -y install git vim
                    rm -rf vim-for-server
                    git clone https://github.com/wklken/vim-for-server.git
                    mv ~/.vimrc ~/.vimrc_bak
                    mv vim-for-server/vimrc ~/.vimrc
                    rm -rf vim-for-server
                    whiptail --title "Success" --msgbox "
            Install & config complete!
            安装配置完成！
                    " 10 60
                    ;;
                * )
                    chVim
            esac

        else
            chVim
        fi
            ;;
        c )
            if(whiptail --title "Yes/No Box" --yesno "
Remove Config?
确认要还原配置？
                " --defaultno 10 60) then
                cp ~/.vimrc.bak ~/.vimrc
                whiptail --title "Success" --msgbox "
Done
已经完成配置
                " 10 60
            else
                chVim
            fi
    esac
else
    exit 
fi
}

chVim