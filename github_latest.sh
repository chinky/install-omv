# Get the version for the latest release of a package provided via GitHub Releases
# Usage: gh_latest_ver USER REPO
# return:github last release version ,like `v0.30.1`
gh_latest_ver() {
  wget -qO- "https://api.github.com/repos/$1/$2/releases/latest" | # Get latest release from GitHub api
    grep '"tag_name":' |                                            # Get tag line
    sed -E 's/.*"([^"]+)".*/\1/'                                    # Pluck JSON value
}


# Gets the download url for the latest release of a package provided via GitHub Releases
# Usage: gh_latest_url USER REPO [PATTERN]
# return:github last release file url ,exaple:
# gh_latest_ur gsamokovarov jump amd64.deb
# `https://github.com/gsamokovarov/jump/releases/download/v0.30.1/jump_0.30.1_amd64.deb`
gh_latest_url() {
  wget -qO- "https://api.github.com/repos/$1/$2/releases/latest" | # Get latest release from GitHub api
  jq -r ".assets[] | select(.name | contains(\"$3\")) | .browser_download_url"
}
