#!/bin/bash

#usage:
#rm -vf install-omv1.sh &&wget https://gitee.com/chinky/Install-OMV/raw/master/install-omv1.sh &&/bin/bash install-omv1.sh

###########################################################################################
### Variables

LOGFILE="install.log"
BASEDIR=$(
	cd "$(dirname "$0")"
	pwd
)

if [ -f $BASEDIR/config ]; then
	source $BASEDIR/config
fi
###########################################################################################
### Functions
f_aborted() {
	whiptail --title "Script cancelled" --backtitle "${BACKTITLE}" --msgbox "         Aborted!" 7 32
	exit 0
}

# simple log-output
f_log() {
	echo "$1" >>$LOGFILE
	echo "$1"
}

###########################################################################################
### Begin of script
f_log "========================================="
if [[ $(id -u) -ne 0 ]]; then
	f_log "This script must be executed as root or using sudo."
	exit 99
fi

if [ -f /etc/os-release ]; then
	. /etc/os-release
else
	f_log "ERROR: I need the file /etc/os-release to determine what my distribution is..."
	exit 98
fi

case $VERSION_CODENAME in
buster)
	confCmd="omv-salt deploy run"
	ntp="chrony"
	omvCodename="usul"
	phpfpm="phpfpm"
	version=5
	;;

*)
	echo "This script only works on Debian buster"
	exit 1
	;;
esac

#set -e

#Add OMV source.list and Update System
f_log "Modify source.list"
sed -i 's/deb.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
sed -i 's|security.debian.org/debian-security|mirrors.ustc.edu.cn/debian-security|g' /etc/apt/sources.list
sed -i 's/deb.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list.d/openmediavault-kernel-backports.list
apt-get update
apt-get upgrade -y

# install  git screen vim ...
f_log "install git  screen vim ..."
apt-get -y install screen git
source $BASEDIR/chvim.sh

# install bash-it
f_log "Install Bash-it ..."
source $BASEDIR/bashit.sh

# install qemu-guest-agent (KVM)
if [[ $(systemd-detect-virt) == "kvm" ]]; then
	apt-get -y install qemu-guest-agent
fi

#copy user config files
f_log "Copy user config files"
find $BASEDIR/debian/ -type f -exec chmod 644 {} \;
find $BASEDIR/debian/root -type d -exec chmod 700 {} \;
chown -R root:root debian/root 
cp -av $BASEDIR/debian/root /root

# check if openmediavault is install properly
omvInstall=$(dpkg -l | awk '$2 == "openmediavault" { print $1 }')
if [[ ! "${omvInstall}" == "ii" ]]; then
	echo "openmediavault package failed to install or is in a bad state."
	exit 3
fi
omv-confdbadm populate
. /etc/default/openmediavault
. /usr/share/openmediavault/scripts/helper-functions

# remove backports from sources.list to avoid duplicate sources warning
sed -i "/${VERSION_CODENAME}-backports/d" /etc/apt/sources.list

# install OMV extras
f_log "Install OMV extras ..."
wget -O - https://github.com/OpenMediaVault-Plugin-Developers/packages/raw/master/install | bash

sed -i 's/deb.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list.d/openmediavault-kernel-backports.list
apt-get update

# #omv source list
# f_log "Modify omv source_list"
# #openmediavault.list
# source_list="/etc/apt/sources.list.d/openmediavault.list"
# # coment origin
# if [ -f "${source_list}" ]; then
# 	sed -i 's/^#/##/g;s/^[^#]/# &/g' ${source_list}
# fi
# cat > /etc/apt/sources.list.d/openmediavault.list <<- EOF
# deb http://packages.openmediavault.org/public $RELEASE_OMV main
# #deb https://openmediavault.github.io/packages/ $RELEASE_OMV main
# ## Uncomment the following line to add software from the proposed repository.
# # deb http://packages.openmediavault.org/public $RELEASE_OMV-proposed main
# # deb https://openmediavault.github.io/packages/ $RELEASE_OMV-proposed main

# ## This software is not part of OpenMediaVault, but is offered by third-party
# ## developers as a service to OpenMediaVault users.
# # deb http://packages.openmediavault.org/public $RELEASE_OMV partner
# EOF

# #openmediavault-kernel-backports.list
# source_list="/etc/apt/sources.list.d/openmediavault-kernel-backports.list"
# # coment origin
# if [ -f "${source_list}" ]; then
# 	sed -i 's/^#/##/g;s/^[^#]/# &/g' ${source_list}
# fi

# f_log "upgrade ..."
# apt-get update -y
# apt-get upgrade -y

# f_log "install openmediavault-docker-gui"
# apt-get install openmediavault-docker-gui
# systemctl daemon-reload

# # enable ssh,
# f_log "enable ssh,"
# omv_config_update "/config/services/ssh/enable"  "1"
# omv_config_update "/config/services/ssh/compression"  "0"
# /usr/sbin/omv-mkconf ssh

# # enable ntp
# f_log "enable ntp"
# omv_config_update "/config/system/time/ntp/enable" "1"
# /usr/sbin/omv-mkconf ntp

# # fix timezone
# f_log "fix timezone"
# omv_config_update "/config/system/time/timezone"  "Asia/Shanghai"
# /usr/sbin/omv-mkconf timezone

# # init OMV
# f_log  "init OMV"
# /usr/sbin/omv-initsystem
