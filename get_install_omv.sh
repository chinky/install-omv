#!/bin/bash

#usage:
# wget -O- https://gitee.com/chinky/install-omv/raw/master/get_install_omv.sh|bash 

set -e

if [ ! -f "/usr/bin/git" ]; then
  apt-get update -y
  apt-get -y install git 
fi

cd ~
rm -fr install-omv
git clone https://gitee.com/chinky/install-omv.git --depth=1

cd install-omv
sudo chmod a+x *.sh
#/bin/bash install_openmediavault.sh