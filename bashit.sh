#!/bin/bash

base_dir=$(
	cd "$(dirname "$0")"
	pwd
)

rm -fr ~/.bash_it
git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash_it
~/.bash_it/install.sh

echo install jump
source  $base_dir/github_latest.sh
wget -O jump.deb $(gh_latest_url gsamokovarov jump amd64.deb)
dpkg -i jump.deb
apt-get -f install
rm -f jump.deb

# config bash it
. ~/.bashrc
bash-it enable plugin git docker docker-compose jump
bash-it enable alias git
bash-it enable completion git docker docker-compose ssh


